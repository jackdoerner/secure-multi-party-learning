#!/usr/bin/python

import gmpy2
import math, os

readfile = os.path.dirname(os.path.realpath(__file__)) + "/ofixed_constants.h.template"
writefile = os.path.dirname(os.path.realpath(__file__)) + "/ofixed_constants.h"

gmpy2.get_context().precision = 1024
inputprec = 256
finalprec = 256

normalizeintbits = int(math.floor(math.log(inputprec,2)))

normalizefactors = "{\n\t"
for ii in range(1,inputprec):
	a = gmpy2.mpfr(1L << ii)
	b = gmpy2.log(a)
	c = b * gmpy2.mpfr(1L << (finalprec-1))
	d = long(c)
	e = d.__format__('0' + str((inputprec+normalizeintbits)/4) + 'X')
	for jj in range(((inputprec+normalizeintbits)/8)-1,-1,-1):
		normalizefactors += "0x" + e[2*jj:2*jj+2] + ","
	normalizefactors += "\n\t"
normalizefactors = normalizefactors[:-3] + "\n}"

logcomponents = "{\n\t"
for ii in range(0,inputprec):
	a = gmpy2.mpfr(1L << ii)
	b = gmpy2.log(1L + 1L/a)
	c = b * gmpy2.mpfr(1L << (finalprec))
	d = long(c)
	e = d.__format__('0' + str(inputprec/4) + 'X')
	for jj in range((inputprec/8)-1,-1,-1):
		logcomponents += "0x" + e[2*jj:2*jj+2] + ","
	logcomponents += "\n\t"
logcomponents = logcomponents[:-3] + "\n}"

with open(readfile, 'r') as content_file:
    content = content_file.read()
    content = content.replace("NORMALIZE_INT_BITS_TEMPLATE", str(normalizeintbits))
    content = content.replace("INPUT_PRECISION_TEMPLATE", str(inputprec))
    content = content.replace("OUTPUT_PRECISION_TEMPLATE", str(finalprec))
    content = content.replace("NORMALIZE_FACTORS_TEMPLATE", str(normalizefactors))
    content = content.replace("LOG_COMPONENTS_TEMPLATE", str(logcomponents))
    with open(writefile, 'w') as output_file:
    	output_file.write(content)

