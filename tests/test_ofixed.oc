#include <obliv.oh>
#include <math.h>
#include "ofixed.oh"
#include "test_generic.h"

#define LOG2LL(X) ((unsigned) (8*sizeof (unsigned long long) - __builtin_clzll((X)) - 1))

static const char TESTNAME[] = "ofixed";

char* get_test_name() {
	return TESTNAME;
}

char* get_supplementary_options_string() {
	return NULL;
}

struct option* get_long_options() {
	return NULL;
}

void print_supplementary_help() {}

double sigmoid(double x) {
     double exp_val = exp((double) -x);
     double result = 1.0 / (1.0 + exp_val);
     return result;
}

void test_main(void*varg) {

	for (size_t ii = 0; ii < 10; ii++) {
		size_t p = 4*8-1;


		double fMin = log(fixed_to_double(0x01ll, p));
		double fMax = log(fixed_to_double(0x7FFFFFFFFFFFFFFFll, p));
		double raw_input = (double)ocBroadcastInt(rand(),1) / RAND_MAX;
		raw_input = fMin + raw_input * (fMax - fMin);
		raw_input = exp(raw_input);

		double expected_output = log(raw_input);
		
		size_t pout = 0;

		printf("ofixed_ln\n");
		printf("\tInput: %.17g\n", raw_input);
		printf("\tRaw Input: %016llX\n", double_to_fixed(raw_input, p));

		obliv fixed_t input = double_to_fixed(raw_input, p);

		obig a;
		obig b;
		obig_init(&a, 8);
		obig_init(&b, 8);
		obig_import_onative(&a, input);
		ofixed_ln(&b, &pout, a, p);

		obliv fixed_t oexported = obig_export_onative(b);
		fixed_t raw_output;
		revealOblivLLong(&raw_output, oexported, 0);
		double output = fixed_to_double(raw_output, pout);

		printf("\tExpected: %.17g\n", expected_output);
		printf("\tActual:   %.17g\n", output);

		printf("\tRaw Expected: %016llX\n", double_to_fixed(expected_output, pout));
		printf("\tRaw Actual:   %016llX\n", raw_output);
		printf("\n");

		obig_free(&a);
		obig_free(&b);
	}

	for (size_t ii = 0; ii < 10; ii++) {
		size_t p = 7*8+4;

		double fMin = fixed_to_double(0x8000000000000000ll, p);
		double fMax = fixed_to_double(0x7FFFFFFFFFFFFFFFll, p);
		double raw_input = (double)ocBroadcastInt(rand(),1) / RAND_MAX;
		raw_input = fMin + raw_input * (fMax - fMin);

		double expected_output = exp(raw_input);
		
		size_t pout = 0;

		printf("ofixed_exp\n");
		printf("\tInput: %.17g\n", raw_input);
		printf("\tRaw Input: %016llX\n", double_to_fixed(raw_input, p));

		obliv fixed_t input = double_to_fixed(raw_input, p);

		obig a;
		obig b;
		obig_init(&a, 8);
		obig_init(&b, 8);
		obig_import_onative(&a, input);
		obliv bool success = ofixed_exp(&b, &pout, a, p);

		obliv fixed_t oexported = obig_export_onative(b);
		fixed_t raw_output;
		revealOblivLLong(&raw_output, oexported, 0);
		double output = fixed_to_double(raw_output, pout);

		printf("\tExpected: %.17g\n", expected_output);
		printf("\tActual:   %.17g\n", output);

		printf("\tRaw Expected: %016llX\n", double_to_fixed(expected_output, pout));
		printf("\tRaw Actual:   %016llX\n", raw_output);

		bool tempbool;
		revealOblivBool(&tempbool, success, 0);
		printf("\tOverflow:   %d\n", !tempbool);
		printf("\n");

		obig_free(&a);
		obig_free(&b);
	}

	for (size_t ii = 0; ii < 10; ii++) {
		size_t p = 7*8+4;

		double fMin = fixed_to_double(0x8000000000000000ll, p);
		double fMax = fixed_to_double(0x7FFFFFFFFFFFFFFFll, p);
		double raw_input = (double)ocBroadcastInt(rand(),1) / RAND_MAX;
		raw_input = fMin + raw_input * (fMax - fMin);

		double expected_output = sigmoid(raw_input);
		
		size_t pout = 0;

		printf("ofixed_sigmoid\n");
		printf("\tInput: %.17g\n", raw_input);
		printf("\tRaw Input: %016llX\n", double_to_fixed(raw_input, p));

		obliv fixed_t input = double_to_fixed(raw_input, p);

		obig a;
		obig b;
		obig_init(&a, 8);
		obig_init(&b, 8);
		obig_import_onative(&a, input);
		obliv bool success = ofixed_sigmoid(&b, &pout, a, p);

		obliv fixed_t oexported = obig_export_onative(b);
		fixed_t raw_output;
		revealOblivLLong(&raw_output, oexported, 0);
		double output = fixed_to_double(raw_output, pout);

		printf("\tExpected: %.17g\n", expected_output);
		printf("\tActual:   %.17g\n", output);

		printf("\tRaw Expected: %016llX\n", double_to_fixed(expected_output, pout));
		printf("\tRaw Actual:   %016llX\n", raw_output);

		bool tempbool;
		revealOblivBool(&tempbool, success, 0);
		printf("\tOverflow:   %d\n", !tempbool);
		printf("\n");

		obig_free(&a);
		obig_free(&b);
	}

}