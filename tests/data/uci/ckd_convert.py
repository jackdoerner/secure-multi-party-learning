f= open("chronic_kidney_disease.arff", 'r')
lines = []
for line in f:
	if (("@" in line) or ("?" in line)):
		continue
	else:
		lines.append(line)
f.close()

vals = []
ranges = []
for ii in range(25):
	vals.append([])
	ranges.append([float("inf"),-float("inf")])
for line in lines:
	line = line.strip()
	line = line.split(",")
	if len(line) != 25:
		continue
	else:
		for ii in range(len(line)):
			if (line[ii] not in vals[ii]):
				try:
					v = float(line[ii].strip())
					if v < ranges[ii][0]:
						ranges[ii][0] = v
					elif v > ranges[ii][1]:
						ranges[ii][1] = v
				except ValueError:
					vals[ii].append(line[ii].strip())

newlines = ""
for line in lines:
	line = line.strip()
	line = line.split(",")
	newline = ""
	if len(line) != 25:
		continue
	else:
		for ii in range(len(vals)):
			if len(vals[ii]) > 0:
				newline += str(vals[ii].index(line[ii]))
			else:
				newline += str((float(line[ii].strip()) - ranges[ii][0])/(ranges[ii][1] - ranges[ii][0]))
			newline += " "
	newlines += newline + "\n"

f= open("chronic_kidney_disease.data", 'w')
f.write(newlines)
f.close()



