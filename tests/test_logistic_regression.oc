#include <obliv.oh>
#include <math.h>
#include "ofixed.oh"
#include "logistic_regression.oh"
#include "test_generic.h"
#include "time.h"

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>

#define DATAFILE "../../tests/data/uci/chronic_kidney_disease.data"
#define DATAROWS 157
#define DATACOLUMNS 24
#define DATADIGITS 4
#define PRECISION 24
#define ITERATIONS 5
#define LEARNING_RATE 3

static const char TESTNAME[] = "logistic_regression";

char* get_test_name() {
	return TESTNAME;
}

char* get_supplementary_options_string() {
	return NULL;
}

struct option* get_long_options() {
	return NULL;
}

void print_supplementary_help() {}

double sigmoid(double x) {
	 double exp_val = exp((double) -x);
	 double result = 1.0 / (1.0 + exp_val);
	 return result;
}

uint64_t current_timestamp() {
	struct timeval tv;
	gettimeofday(&tv,NULL);
	return (uint64_t)tv.tv_sec*(uint64_t)1000000+(uint64_t)tv.tv_usec;
}

void test_main(void*varg) {

	FILE * infilep;
	char * line = NULL;
	size_t len = 0;
	ssize_t read;

	char *p;
	char *delim = " ";

	infilep = fopen(DATAFILE, "r");
	if (infilep == NULL) {
		printf("Failed to open data file.\n");
		exit(EXIT_FAILURE);
	}

	obig data[DATAROWS * DATACOLUMNS];
	for (size_t ii = 0; ii < DATAROWS * DATACOLUMNS; ii++) {
		obig_init(&data[ii], DATADIGITS);
	}

	obliv bool labels[DATAROWS];
	bool publabels[DATAROWS];

	for (size_t jj = 0; jj < DATAROWS; jj++) {
		read = getline(&line, &len, infilep);
 
		p = strtok(line,delim);
		for(size_t ii = 0; ii < DATACOLUMNS + 1; ii++) {
			double pf = atof(p);
			if (ii < DATACOLUMNS) {
				int64_t pt = double_to_fixed(pf, PRECISION);
				obliv int64_t pto;
				pto = feedOblivLLong(pt, 1);
				obig_import_onative_signed(&data[jj * DATACOLUMNS + ii], pto);
			} else {
				publabels[jj] = atoi(p);
				labels[jj] = feedOblivBool(atoi(p), 2);
			}
			p = strtok(NULL,delim);
		} 
	}

	obig weights[DATACOLUMNS];
	for (size_t ii = 0; ii < DATACOLUMNS; ii++) {
		obig_init(&weights[ii], DATADIGITS);
	}

	obig rate;
	obig_init(&rate, DATADIGITS);
	obig_import_onative_signed(&rate, double_to_fixed(LEARNING_RATE, PRECISION));

	int64_t ygc = -yaoGateCount();
	int64_t runtime = -current_timestamp();

	smpl_lr_gradient_descent(weights, data, labels, rate, PRECISION, DATACOLUMNS, DATAROWS, ITERATIONS);

	ygc += yaoGateCount();
	runtime += current_timestamp();

	printf("Training benchmark: %d microseconds, %d gates\n", runtime,ygc);

	obig result;
	obig_init(&result, DATADIGITS);
	size_t outprecision;
	ygc = 0;
	runtime = 0;

	uint32_t correct = 0;
	uint32_t negatives = 0;
	uint32_t fn = 0;
	uint32_t positives = 0;
	uint32_t fp = 0;

	for (size_t jj = 0; jj < DATAROWS; jj++) {
		ygc -= yaoGateCount();
		runtime -= current_timestamp();
		smpl_lr_classify(&result, &outprecision, weights, &data[DATACOLUMNS*jj], PRECISION, DATACOLUMNS);
		ygc += yaoGateCount();
		runtime += current_timestamp();

		obliv int64_t tempo = obig_export_onative_signed(result);
		int64_t temp;
		revealOblivLLong(&temp,tempo,0);
		double tempf = fixed_to_double(temp, outprecision);

		if (publabels[jj]) positives++;
		else negatives++;

		if (tempf < 0.5 && publabels[jj] == 1) fn++;
		else if (tempf >= 0.5 && publabels[jj] == 0) fp++;
		else correct++;
	}
	obig_free(&result);

	printf("Classification benchmark: %d microseconds avg, %d gates avg\n", runtime/(DATAROWS), ygc/(DATAROWS));
	printf("Recall performance:\n");
	printf("\tCorrect: %d/%d, %.2f%%\n",correct,DATAROWS,((double)correct)/DATAROWS * 100.0);
	printf("\tFalse Positives: %d/%d, %.2f%%\n",fp,negatives,((double)fp)/negatives * 100.0);
	printf("\tFalse Negatives: %d/%d, %.2f%%\n",fn,positives,((double)fn)/positives * 100.0);

	for (size_t ii = 0; ii < DATACOLUMNS; ii++) {
		obig_free(&weights[ii]);
	}

	for (size_t ii = 0; ii < DATAROWS * DATACOLUMNS; ii++) {
		obig_free(&data[ii]);
	}

	fclose(infilep);
	if (line)
		free(line);
	exit(EXIT_SUCCESS);

}